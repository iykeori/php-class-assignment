<?php
    //test.php
    //This is a line comment
    /*
        This is a block comment
    */

    echo "Hello<br />";
    echo "me\n";

    echo nl2br("\nI am \nhere");


    echo '<br /> This is also a string';
    echo "\n<img src=\"test.png\" style=\"border:none;\" />";
    echo "\n<img src='test.png'style='border:none;' />";
    echo "\n".'<img src="test.png" style="border:none;" />';
    //echo '<img src='test.png' style='border:none;'' />';


    $myVar = "Text";
    $myVar = 115;
    $myVar = 1501;
    $myVar = true;
?>