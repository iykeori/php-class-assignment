<?php
    //3.Create a HTML form that allows a user to enter two numbers. After pressing submit, 
    //use PHP to retrieve the numbers and store them as variables. 
    //Create 4 functions (addThem, subtractThem, multiplyThem, and divideThem) that accepts both variables, 
    //does the appropriate mathematic operation, and sends the result back to the program. 
    //Call each function in your program and display the result of the function calls using the following format: 
    //$x plus $y is $result, $x minus $y is $result2, etc...
    
    //retrieve the data sent via the METHOD (post)
    $Number1 = "";
    $Number2 = "";

    //check that the form has been submitted
    $errors = false;
    if ( isset( $_POST["form1"] ) ) {
        //if it has, retrieve each field
        $Number1 = $_POST["Number1"];
        $Number2 = $_POST["Number2"];

        //check your retrieved data for errors
        
        $error_code = 0;
        if ( $Number1 == null || empty($Number1) ) { 
            $errors = true; 
            $error_code=1;
        }
        if ( $Number2 == null || empty($Number2) ) { 
            $errors = true; 
            $error_code=2;
        }

    }

    //if there are errors redisplay the form
    if (! isset( $_POST["form1"] ) || $errors) { 
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Assignment 1c</title>
    </head>
    <body>
        <p>Please fill in the 2 boxes below with numbers.
        </p>
        <form action="" method="post">

            Number1*: <input type="text" name="Number1" value="<?php echo $Number1; ?>" />
            <?php if ( isset($_POST["form1"]) && empty($Number1)) echo " *required "; ?><br />
            Number2*: <input type="text" name="Number2" value="<?php echo $Number2; ?>" />
            <?php if ( isset($_POST["form1"]) && empty($Number2)) echo " *required "; ?><br />
            <input type="submit" name="form1" value="Submit" />
        </form>
    </body>
</html>
<?php
    } else {
        //otherwise insert into database, etc and 
        //redirect to success page
        echo "$Number1 plus $Number2 = ". addThem($Number1,$Number2). "<br />";
        echo "$Number1 minus $Number2 = ". subtractThem($Number1,$Number2). "<br />";
        echo "$Number1 multiplied by $Number2 = ". multiplyThem($Number1,$Number2). "<br />";
        echo "$Number1 divided by $Number2 = ". divideThem($Number1,$Number2);
    }

    function addThem($x,$y){
        return $x + $y;
    }
    function subtractThem($x,$y){
        return $x - $y;
    }
    function multiplyThem($x,$y){
        return $x * $y;
    }
    function divideThem($x,$y){
        return $x / $y;
    }
?>