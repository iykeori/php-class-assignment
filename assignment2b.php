<?php
    //Create a HTML form that allows a user to choose a pizza size and a number of toppings. 
    //Use a dropdown field for size (extra-large, large, medium, and small) and a checkbox field for each topping 
    //(pepperoni, cheese, olive, pineapple, and ham). Retrieve the user's selections and calculate their overall price based on the following criteria:
    // A small pizza is $9.00
    // A medium pizza is $12.50
    // A large pizza is $15.00
    // An extra large pizza is $17.50
    // Each topping adds $1.00 to the total cost of the order (except cheese is free).
    // Display the size and toppings ordered along with the total cost of the order.
    error_reporting(0);
    ini_set('display_errors', 0);

    //retrieve the data sent via the METHOD (post)
    $pSize = "";
    $pepper =  "";
    $cheese = "";
    $olive = "";
    $pine = "";
    $ham = "";

    //check that the form has been submitted
    $errors = false;
    if ( isset( $_POST["form1"] ) ) {
        //if it has, retrieve each field
        $pSize = $_POST["pSize"];
        $pepper = $_POST["pepper"];
        $cheese = $_POST["cheese"];
        $olive = $_POST["olive"];
        $pine = $_POST["pine"];
        $ham = $_POST["ham"];

        if ($pSize == "extraLarge"){
            $pSize = 17.50;
        } elseif ($pSize == "large"){
            $pSize = 15.00;
        } elseif ($pSize == "medium"){
            $pSize = 12.50;
        } elseif ($pSize == "small"){
            $pSize = 9.00;
        }

        //check your retrieved data for errors
        
        $error_code = 0;
        if ( $pSize == null || empty($pSize) ) { 
            $errors = true; 
            $error_code=1;
        }
        //if ( $pepper == null || empty($pepper) || empty($cheese) || empty($olive) || empty($pine) || empty($ham)) { 
        if ( $pepper == null && $cheese == null && $olive == null && $pine == null && $ham == null) { 
            $errors = true; 
            $error_code=2;
        }
        if ($pepper != null && $pepper == 1) {
            //pepperoni was checked
            $pepper=1.00;
        }  else {
            $pepper=0.00;
        }
        if ($cheese != null && $cheese == 1) {
            $cheese=0.00;
        }  else {
            $cheese=0.00;
        } 
        if ($olive != null && $olive == 1) {
            $olive=1.00;
        }  else {
            $olive=0.00;
        } 
        if ($ham != null && $ham == 1) {
            $ham=1.00;
        }  else {
            $ham=0.00;
        } 
        if ($pine != null && $pine == 1) {
            $pine=1.00;
        }  else {
            $pine=0.00;
        }
        

    }

    //if there are errors redisplay the form
    if (! isset( $_POST["form1"] ) || $errors) { 
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Assignment 2b</title>
    </head>
    <body>
        <p>Fill the boxes below.
        </p>

        <form action="" method="post">
            Pizza Size: <select name="pSize" value="<?php echo $pSize; ?>" >
                        <option value=""></option>  
                        <option value="extraLarge">Extra-Large Pizza -> $17.50</option>
                        <option value="large">Large Pizza -> $15.50</option>
                        <option value="medium">Medium Pizza -> $12.00</option>
                        <option value="small">Small Pizza -> $9.00</option>
                    </select><br />
                    <?php if ( isset($_POST["form1"]) && empty($pSize)) echo " *required "; ?><br />
                    
            <label>Pepperoni
            <input type="checkbox" checked="checked" name="pepper" value="1" >
            </label>

            <label>cheese
            <input type="checkbox" name="cheese" value="1" >
            </label>

            <label>olive
            <input type="checkbox" name="olive" value="1" >
            </label>

            <label>pineapple
            <input type="checkbox" name="pine" value="1" >
            </label>
            <label>Ham
            <input type="checkbox" name="ham" value="1" >
            </label>
            <input type="submit" name="form1" value="Submit" /><br />
        </form>
    </body>
</html>
<?php
    } else {
        //redirect to success page
        echo "Overall Pizza Price = ". overallPrice($pSize,$pepper,$cheese,$pine,$olive,$ham). "<br />";

    }
//Function
    function overallPrice($a,$b,$c,$d,$e,$f){
        //echo "$a,$b,$c,$d,$e,$f";
        return $a+$b+$c+$d+$e+$f;
    }

    
?>