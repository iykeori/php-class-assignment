<?php

    include("Person.php");

    class Employee extends Person  {

        private $hoursWorked;
        private $rateOfPay;
        
        function getHoursWorked() { return $this->hoursWorked;}
        function setHoursWorked ( $temp ) { $this->hoursWorked = $temp;}

        function getRateOfPay() { return $this->rateOfPay;}
        function setRateOfPay( $temp ) { $this->rateOfPay = $temp;}  
        
        
        function __construct() {
            $parameters = func_get_args();

            if (count($parameters) == 6) {
                parent::__construct($parameters[0], $parameters{1},$parameters[2], $parameters{3}); //super(); in java
                $this->hoursWorked = 0;
                $this->rateOfPay = 0;
            


            } else if (count($parameters) == 2) {
                parent::__construct($parameters[0], $parameters{1}); //super(); in java
                $this->hoursWorked = 0;
                $this->rateOfPay = 0; 
                
            } else {
                parent::__construct(); //super(); in java
                $this->hoursWorked = 0;
                $this->rateOfPay = 0;
            }
        }

        function __destruct(){
            
            
        }

        function display(){
            echo "id:".$this->id."<br />";
            echo "firstName: $this->firstName <br />";
            echo "lastName: $this->lastName <br />";
            echo "address: $this->address <br />";
            echo "HoursWorked: $this->hoursWorked <br />";
            echo "RateofPay: $this->rateOfPay <br />";
        
            
        }
        
    }