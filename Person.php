<?php
    //1.class skeleton
    // data members
    // 3. getters and setters
    // 4. constructor (s)
    // 5. display function
    // 6.any other functions
    // 7.Test in program

    Class Person {
        protected $id;
        protected $firstName;
        protected $lastName;
        protected $address;

        function getID() { return $this->id; }
        function setID ( $temp ) { $this->id = $temp;}

        function getFirstName() { return $this->firstName; }
        function setFirstName ( $temp ) { $this->firstName = $temp;}

        function getLastName() { return $this->lastName; }
        function setLastName ( $temp ) { $this->lastName = $temp;}
        
        function getAddress() { return $this->lastName; }
        function setAddress( $temp ) { $this->lastName = $temp;}

        function display() {
            echo "id: $this->id<br /> ";
            echo "firstName: $this->firstName <br />";
            echo "lastName: $this->lastName<br /> ";
            echo "address: $this->address <br />";
        }

        function __construct() {

            $parameters = func_get_args();
            if ( count($parameters) == 4){
                $this->id = $parameters[0];
                $this->firstName= $parameters[1];
                $this->lastName= $parameters[2];
                $this->address = $parameters[3];
            } else if ( count($parameters) == 2){
                $this->id = 0;
                $this->firstName= $parameters[0];
                $this->lastName= $parameters[1];
                $this->address = "";
            } else {
                //default constructor
                $this->id = 0;
                $this->firstName= "";
                $this->lastName= "";
                $this->address = "";               
            }
        }

        function __destruct(){
        
            
        }
    }