<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
   
    <title>My Favourite Movies</title>
  </head>
  <body>

      <?php
        if(file_exists("fav_movies.xml")){
          $myFile = fopen("fav_movies.xml","r");
          $data = fread($myFile, filesize("fav_movies.xml"));
          fclose($myFile);
          
          echo "<table class='table'>";
          $count = 1;

          echo "<tr>";
          $xml = simplexml_load_file("fav_movies.xml");

          foreach ($xml->movie as $movieElement){
            //$count++;
            if ($count == 4) { 
              
              echo "<tr>";
            }
            echo "<th>" ;
  
            echo "<img src='" .$movieElement->picture."' width='150px' height='200px' /><br/>";      
            echo "<h1>".$movieElement->title."(".$movieElement->year.")</h1>"; 
            echo "Director: ".$movieElement->director."<br/>"; 
            echo "Main Actor: ".$movieElement->mainactor."<br/>"; 
            echo "Genre: ".$movieElement->genre."<br/>"; 
            echo "<br/> <br/>";
            echo "</th>";

                if ($count  == 3) { 
              
                  echo "</tr>";
                  $count = 0;
                }
                $count++;    
          }
              echo "</tr>";
              echo "</table>";
        }else {
          echo "File not found";
        }
      ?>
  </body>
</html>