<!-- Create a php script to store your 10 favorite movies in an xml file. 
Your xml file must contain the following elements: Title, Picture 
(use URL to live picture on web), Director, MainActor, IMDB (url from imdb), Year, and Genre. 
Your php program should save your xml data to fav_movies.xml. 
Use an XML validation tool to verify your xml file. 
If there are errors, fix and test until no errors remain. -->
<?php
    $txt = "<?xml version=\"1.0\" ?>\n";
    $txt .= "<movies>\n";

    $txt .= "<movie id=\"mov101\">
                <director> Kemi Adetiba </director>
                <title> Wedding Party </title>
                <picture>https://m.media-amazon.com/images/M/MV5BY2VmMjI0ZWYtZDY4Yy00ZmQwLWIzNTgtZTZjYjVjYTJiMmUyL2ltYWdlL2ltYWdlXkEyXkFqcGdeQXVyNzA1MjQxMTQ@._V1_SY1000_CR0,0,666,1000_AL_.jpg</picture>
                <mainactor>Adesua Etomi-Wellington </mainactor>
                <imdb> https://www.imdb.com/title/tt5978822/ </imdb>
                <year>2017 </year>
                <genre> Comedy </genre>
            </movie>\n";  

    $txt .= "<movie id=\"mov102\">
                <director> Kemi Adetiba  </director>
                <title> Wedding Party 2 </title>
                <picture>https://m.media-amazon.com/images/M/MV5BNGFkMDJlZjItZDFjMi00ZTdjLWIxYTktZDNlYTJlZTExMDQ3XkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_.jpg</picture>
                <mainactor> Nonso Onwuka </mainactor>
                <imdb> https://www.imdb.com/title/tt7827944/?ref_=tt_sims_tti  </imdb>
                <year>2018 </year>
                <genre> Comedy </genre>
            </movie>\n";     

    $txt .=  "<movie id=\"mov103\">
                <director> Ishaya Bako  </director>
                <title>Road to yesterday  </title>
                <picture>https://m.media-amazon.com/images/M/MV5BMTU3NzEyMzY4Nl5BMl5BanBnXkFtZTgwNjU1ODM5NjE@._V1_SY1000_CR0,0,708,1000_AL_.jpg  </picture>
                <mainactor> Genevieve Nnaji </mainactor>
                <imdb>https://www.imdb.com/title/tt4871120/?ref_=tt_sims_tti  </imdb>
                <year>2015 </year>
                <genre>Thriller </genre>
            </movie>\n";        

    $txt .=  "<movie id=\"mov104\">
                <director> Toka McBaror  </director>
                <title> Merry men  </title>
                <picture> https://m.media-amazon.com/images/M/MV5BMDljNjk3MWYtYjA4Zi00MDUyLWI2ZmUtOTQ3ZTI4YWUxYTI5XkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_.jpg </picture>
                <mainactor> Ramson Nuel </mainactor>
                <imdb> https://www.imdb.com/title/tt9837502/?ref_=nv_sr_1?ref_=nv_sr_1 </imdb>
                <year>2018 </year>
                <genre> Comedy </genre>
            </movie>\n";

    $txt .=  "<movie id=\"mov105\">
                <director> Genevieve Nnaji  </director>
                <title>Lion heart  </title>
                <picture>https://m.media-amazon.com/images/M/MV5BYTQ1NThiOTgtODMyMS00ZGNlLTkwMmQtMzJiZmVkZTg0ZjczXkEyXkFqcGdeQXVyOTA3NzgxODQ@._V1_.jpg  </picture>
                <mainactor> Pete Edochie </mainactor>
                <imdb> https://www.imdb.com/title/tt7707314/?ref_=tt_sims_tti </imdb>
                <year>2019 </year>
                <genre> Comedy </genre>
            </movie>\n"; 

    $txt .=  "<movie id=\"mov106\">
                <director> Niyi Akinmolayan </director>
                <title> Chief daddy  </title>
                <picture>https://m.media-amazon.com/images/M/MV5BNzhkZTIzN2QtNjhmNC00OGFlLWFmMjYtZjJhYzM1Zjg4OTkzXkEyXkFqcGdeQXVyNzgzMTc2OTA@._V1_SY1000_SX1000_AL_.jpg  </picture>
                <mainactor>Falz  </mainactor>
                <imdb> https://www.imdb.com/title/tt9652322/?ref_=tt_sims_tti  </imdb>
                <year>2019 </year>
                <genre>Comedy  </genre>
            </movie>\n"; 

    $txt .=  "<movie id=\"mov107\">
                <director>Tope Oshin   </director>
                <title> Up North </title>
                <picture> https://m.media-amazon.com/images/M/MV5BMzRmZjhmZDctZjA2My00ZGE4LWE2YjEtOGI4NDE1OGIzYjdjXkEyXkFqcGdeQXVyODk1MTM1NjI@._V1_SY1000_SX750_AL_.jpg  </picture>
                <mainactor> Banky w  </mainactor>
                <imdb>https://www.imdb.com/title/tt8631800/?ref_=tt_sims_tti  </imdb>
                <year>2018 </year>
                <genre> Drama </genre>
            </movie>\n"; 

    $txt .=  "<movie id=\"mov108\">
            <director>Paul Scheuring</director>
            <title>Prison Break  </title>
            <picture>https://m.media-amazon.com/images/M/MV5BMTg3NTkwNzAxOF5BMl5BanBnXkFtZTcwMjM1NjI5MQ@@._V1_.jpg </picture>
            <mainactor> Wentworth Miller </mainactor>
            <imdb> https://www.imdb.com/title/tt0455275/?ref_=nv_sr_1?ref_=nv_sr_1 </imdb>
            <year>2005 </year>
            <genre> Action </genre>
        </movie>\n";            
   

    $txt .=  "<movie id=\"mov109\">
                <director>Alex Kendrick </director>
                <title> War Room </title>
                <picture>https://m.media-amazon.com/images/M/MV5BMTYyNTUxMjQwNF5BMl5BanBnXkFtZTgwNDY5MDIwNTE@._V1_SY1000_CR0,0,674,1000_AL_.jpg  </picture>
                <mainactor>T.C. Stallings </mainactor>
                <imdb>https://www.imdb.com/title/tt3832914/?ref_=nv_sr_1?ref_=nv_sr_1  </imdb>
                <year>2015 </year>
                <genre>Drama  </genre>
            </movie>\n";

    $txt .=  "<movie id=\"mov110\">
                <director> Harold Cronk </director>
                <title> God's NOT Dead </title>
                <picture> https://m.media-amazon.com/images/M/MV5BMjEwNDQ3MzYyOV5BMl5BanBnXkFtZTgwNDE0ODM3MDE@._V1_.jpg </picture>
                <mainactor>  </mainactor>
                <imdb>https://www.imdb.com/title/tt2528814/?ref_=tt_sims_tti  </imdb>
                <year>2014 </year>
                <genre>drama  </genre>
            </movie>\n";
    $txt .= "</movies>";        
    $myFile = fopen("fav_movies.xml", "w");
    fwrite($myFile,$txt);
    fclose($myFile);