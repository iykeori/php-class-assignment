<?php

    //1.class skeleton
    // data members
    // 3. getters and setters
    // 4. constructor (s)
    // 5. display function
    // 6.any other functions
    // 7.Test in program

    Class movie {
        protected $director;
        protected $title;
        protected $picture;
        protected $mainactor;
        protected $imdb;
        protected $year;
        protected $genre;

        function getDirector() { return $this->director; }
        function setDirector ( $temp ) { $this->director = $temp;}

        function getTitle() { return $this->title; }
        function setTitle ( $temp ) { $this->title = $temp;}

        function getPicture() { return $this->picture; }
        function setPicture ( $temp ) { $this->picture = $temp;}
        
        function getMainactor() { return $this->mainactor; }
        function setMainactor( $temp ) { $this->mainactor = $temp;}

        function getImdb() { return $this->imdb; }
        function setImdb( $temp ) { $this->imdb = $temp;}

        function getYear() { return $this->year; }
        function setYear( $temp ) { $this->year = $temp;}

        function getGenre() { return $this->genre; }
        function setGenre( $temp ) { $this->genre = $temp;}

        function display() {
    
                echo "Director: $this->director<br /> ";
                echo "Title: $this->title <br />";
                echo "Picture: $this->picture<br /> ";
                echo "Main Actor: $this->mainactor <br />";
                echo "Imdb: $this->imdb <br />";
                echo "Year: $this->Year <br />";
                echo "Genre: $this->genre <br />";
                echo "<br/> <br/>";
    
        }

        function __construct() {

            $parameters = func_get_args();
            if ( count($parameters) == 7){
                $this->director = $parameters[0];
                $this->title= $parameters[1];
                $this->picture= $parameters[2];
                $this->mainactor = $parameters[3];
                $this->imdb = $parameters[4];
                $this->year = $parameters[5];
                $this->genre = $parameters[6];
            } else if ( count($parameters) == 5){
                
                $this->director = $parameters[2];
                $this->title= $parameters[1];
                $this->picture= $parameters[0];
                $this->mainactor = $parameters[3];
                $this->imdb = "";
                $this->year = 0;
                $this->genre = $parameters[4];
            } else {
                //default constructor
                $this->director = "";
                $this->title= "";
                $this->picture= "";
                $this->mainactor = "";
                $this->imdb = "";
                $this->year = 0;
                $this->genre = "";               
            }
        }

        function __destruct(){
        
            
        }
    }