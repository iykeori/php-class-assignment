<?php

    //retrieve the data sent via the METHOD (post)
    $fName = "";
    $lName = "";
    $title = "";
    $address = "";

    //check that the form has been submitted
    $errors = false;
    if ( isset( $_POST["form1"] ) ) {
        //if it has, retrieve each field
        $fName = $_POST["fName"];
        $lName = $_POST["lName"];
        $title = $_POST["title"];
        $address = $_POST["address"];

        //check your retrieved data for errors
        
        $error_code = 0;
        if ( $fName == null || empty($fName) ) { 
            $errors = true; 
            $error_code=1;
        }
        if ( $lName == null || empty($lName) ) { 
            $errors = true; 
            $error_code=2;
        }
        if ( $title == null || empty($title) ) { 
            $errors = true; 
            $error_code=3;
        }
    }

    //if there are errors redisplay the form
    if (! isset( $_POST["form1"] ) || $errors) { 
?>
<!DOCTYPE html>
<html>
    <head>
        <title>My Form</title>
    </head>
    <body>
        <p>Please fill in this form. 
           Fields marked with an asterisk (*) are 
           required.
        </p>
        <form action="" method="post">
            Title*:  <select name="title" required>
                        <option value=""></option>
                        <option value="Mr"
                        <?php if ($title != null && $title=="Mr") echo " selected "; ?>
                        >Mr</option>
                        <option value="Mrs" 
                        <?php if ($title != null && $title=="Mrs") echo " selected "; ?>
                        >Mrs</option>
                        <option value="Ms"
                        <?php if ($title != null && $title=="Ms") echo " selected "; ?>
                        >Ms</option>
                        <option value="X"
                        <?php if ($title != null && $title=="X") echo " selected "; ?>
                        >X</option>
                    </select>
                    <?php if ( isset($_POST["form1"]) &&  ($title == null || empty($title)) ) echo " *required "; ?>
                    <br />
            First Name*: <input type="text" name="fName" value="<?php echo $fName; ?>" />
            <?php if ( isset($_POST["form1"]) && empty($fName)) echo " *required "; ?><br />
            Last Name*: <input type="text" name="lName" value="<?php echo $lName; ?>" />
            <?php if ( isset($_POST["form1"]) && empty($lName)) echo " *required "; ?><br />
            Address: <input type="text" name="address" value="<?php echo $address; ?>" /><br />
            <input type="submit" name="form1" value="Submit" />
        </form>
    </body>
</html>
<?php
    } else {
        //otherwise insert into database, etc and 
        //redirect to success page
    }

?>