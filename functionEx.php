<?php
    //call the function 3 times
    testfunction();
    testfunction();
    testfunction();

    //define the function
    function testfunction() {
        echo "Hello!<br />";
    }

    // calling function, passing parameters and return value
    $mySum = sumMe("Darren", 50,60.1);
    echo "My sum is $mySum";

    //define the function
    function sumMe($name, $x, $y) {
        echo "HEllo $name!";
        $sum = $x + $y;
        return $sum;
    }

?>