<?php
    //Create a HTML form that allows a user to enter a school mark/grade. After pressing submit, use PHP to execute the following tasks:
        //If no value is entered, print an error message.
        //If the value is numeric, use a nested if statement to display the corresponding letter Grade according to the following:
        //A:  85-100 B:  75-84.99 C:  60-74.99 F:  0-59.99.
        //If the mark is not numeric, use a switch statement to display the mark range the grade corresponds to (see above table).


    //retrieve the data sent via the METHOD (post)
    $score = "";
    //check that the form has been submitted
    $errors = false;
    if ( isset( $_POST["form1"] ) ) {
        //if it has, retrieve each field
        $score = $_POST["score"];
        //check your retrieved data for errors       
        $error_code = 0;
        if ( $score == null || empty($score) ) { 
            $errors = true; 
            $error_code=1;
        }

    }

    //if there are errors redisplay the form
    if (! isset( $_POST["form1"] ) || $errors) { 
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Assignment 2a</title>
    </head>
    <body>
        <p>Please enter Student Grade in the box provided
        </p>
        <form action="" method="post">
            
            Score*: <input type="text" name="score" value="<?php echo $score ; ?>" />
            <?php if (isset($_POST["form1"]) && empty($score )) echo " *required "; ?><br />

            <input type="submit" name="form1" value="Submit" />
        </form>
    </body>
</html>
<?php
    } else {
        //checking for empty value
        if ($score == ""){
            echo "Invalid Entry!";
        // checking if value is not numeric   
        }elseif(is_numeric($score)== false ){
            //Using the SWITCH STATEMENT
            $score=strtoupper($score);
            switch ($score) {
                case A: echo "$score is within the range of 85-100"; break;
                case B: echo "$score is within the range of 75-84.99"; break;
                case C: echo "$score is within the range of 60-74.99"; break;
                case F: echo "$score is within the range of 0-59.99"; break;
                default: echo "NO RANGE ";
            }
                 
        // checking if value is numeric
        }elseif (is_numeric($score)== true){
            //using if statement to compare grade
            if($score >= 85 && $score <=100){
                echo "A";
            }elseif ($score >= 75 && $score <=84.99){
                echo "B";
            }elseif ($score >= 60 && $score <=74.99){
                echo "c";
            }elseif ($score >= 0 && $score <=59.99){
                echo "F";
            }               
        }
    }

?>