<?php

//$animal = array();
$animal = array("cat", "dog","fish","bat","horse","pasta","cow","deer");

print_r($animal);


$isFirst = true;
for ($i=0; $i < count($animal); $i++ ) {

    if ($isFirst) {
        $isFirst = false;
    } else {
        echo ", ";
    }
    echo $animal[$i];

}
$displayAnimal = " ";
for ($i=0; $i < count($animal); $i++ ) {
    $displayAnimal .= $animal[$i].", ";

}
$displayAnimal = substr($displayAnimal, 0, -2);
echo "<br />".$displayAnimal;

foreach ($animal as $quirrel) {
    echo $quirrel. ", ";
}

//associative array => not numerically indexed
$moose = array("an1"=>"cat", "an2"=>"dog",
                "an3"=>"Nemo", "an4"=>"Dory");
print_r($moose);

foreach( $moose as $key => $value) {
    echo "<br />$key => $value";
}

array_push($animal, "gazelle");
$moose["food"] = "zebra";

$str_marvel = "ant man, black panther, iron man, spiderman, deadpool, bob";
$marvel = explode(", ", $str_marvel);
print_r($marvel);

sort($marvel);
echo "sorted: ";
print_r($marvel);
unset($marvel[2]);
echo "snap: ";
print_r($marvel);
$marvel = array_values($marvel);
print_r($marvel);

array_push($marvel, "bob");
array_push($marvel, "bob");
print_r($marvel);
$marvel =array_unique($marvel);
$marvel = array_values($marvel);
print_r($marvel);

//2 dimensional Array
$mylist = array(array("cat", "Felis"),
                array("dog", "Canis"),
                array("cow", "Moo"));

                for ($row =0; $row < count($mylist); $row++) {
                    for ($col = 0; $col < count($mylist[0]); $col++) {
                        echo $mylist[$row][$col]." ";
                    }
                    echo "<br />";
                }

?>

