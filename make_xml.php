<?php
    $txt = "<?xml version=\"1.0\" ?>\n";
    $txt .= "<catalog>\n";

    $txt .= "<book id=\"bk101\">
                <author> Author 1 </author>
                <title> Title 1 </title>
                <genre> Genre 1 </genre>
                <price> price 1 </price>
                <publish_date> Date 1 </publish_date>
            </book>\n";  

    $txt .= "<book id=\"bk102\">
                <author> Author 2 </author>
                <title> Title 2 </title>
                <genre> Genre 2 </genre>
                <price> price 2 </price>
                <publish_date> Date 2 </publish_date>
            </book>\n";    

    $txt .= "<book id=\"bk103\">
                <author> Author 3 </author>
                <title> Title 3 </title>
                <genre> Genre 3 </genre>
                <price> price 3 </price>
                <publish_date> Date 3 </publish_date>
        </book>\n";       

    $txt .= "</catalog>";
    $myFile = fopen("myfile.xml", "w");
    fwrite($myFile,$txt);
    fclose($myFile);