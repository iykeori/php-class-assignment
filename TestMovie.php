<?php
    include("movie.php");

// Create a new php program that uses this class as follows:
// declare an array that will hold your movie data.
// Load the xml file and use a foreach command to loop through each element set.  
// For each element set, store in an instance of your movie class and add to the movies array.
// After all elements have been processed, use a for statement to loop through the array and 
// display the movie data in a HTML table (with rows of 3 elements).

    
    $movie_array = array();
   
        $xml = simplexml_load_file("fav_movies.xml");
        foreach ($xml->movie as $movieElement ){
                   
            //create the new movie
            $instanceMovie = new movie();
            $instanceMovie->setPicture( $movieElement->picture);
            $instanceMovie->setTitle($movieElement->title);
            $instanceMovie->setYear(trim($movieElement->year));
            $instanceMovie->setDirector ($movieElement->director);
            $instanceMovie->setMainactor($movieElement->mainactor); 
            $instanceMovie->setGenre($movieElement->genre); 
            array_push($movie_array, $instanceMovie);
               
        }
        $count = 1;
        echo "<table>";           
          echo "<tr>";
          //loop through the movie_array and displaying it
        for ($i=0; $i < count($movie_array); $i++ ) {
             
            if ($count == 4) {               
              echo "<tr>";
            }

            echo "<th>" ;

              echo "<img src='". $movie_array[$i]->getPicture()."width='150px' height='200px'>"."<br />" ;
              echo "Title: ".$movie_array[$i]->getTitle()."<br />";
              echo  "Year: ".$movie_array[$i]->getYear()."<br />";
              echo  "Director: ".$movie_array[$i]->getDirector()."<br />";
              echo  "Main Actor: ".$movie_array[$i]->getMainactor()."<br />";
              echo  "Genre: ".$movie_array[$i]->getGenre()."<br />";

            echo "</th>";
            

            if ($count  == 3) { 
      
              echo "</tr>";
              $count = 0;
            }

          $count++;        
        }
          echo "</tr>";
        echo "</table>";
           
      ?>