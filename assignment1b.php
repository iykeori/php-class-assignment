<?php

    //retrieve the data sent via the METHOD (post)
    $dDate = "";
    //check that the form has been submitted
    $errors = false;
    if ( isset( $_POST["form1"] ) ) {
        //if it has, retrieve each field
        $dDate = $_POST["dDate"];
        //check your retrieved data for errors       
        $error_code = 0;
        if ( $dDate == null || empty($dDate) ) { 
            $errors = true; 
            $error_code=1;
        }

    }

    //if there are errors redisplay the form
    if (! isset( $_POST["form1"] ) || $errors) { 
?>
<!DOCTYPE html>
<html>
    <head>
 <!-- Required meta tags -->
 <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
       
        <title>My Form</title>


        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

        <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">


    </head>
    <body>
        <p>Please enter a number in the box provided
        </p>
        <form action="" method="post">
        <div class="form-group col-md-2">  
            Date*: <input type="text" class="form-control" name="dDate" value="<?php echo $dDate; ?>" placeholder="YYYY-MM-DD" />
            <?php if ( isset($_POST["form1"]) && empty($dDate)) echo " *required "; ?><br />
            <input type="submit" class="btn btn-danger" name="form1" value="Submit" />
       </div>
        </form>
    </body>
</html>
<?php
    } else {
        //otherwise insert into database, etc and 
        //redirect to success page

        $date1 = new DateTime('2016-06-30'); //given date
        $date2 = new DateTime($dDate); // date typed by user
        $days  = $date2->diff($date1)->format('%a');

        //echo "Number of days between data entered and June 30, 2016 = ".round($datediff / (60 * 60 * 24));
        echo $days ."days in between";
    }

?>