<?php
    include_once("dbinfo.php");

    //localhost:8888/phpMyAdmin.php

    // Establish connection
    //$conn = mysqli_connect($db_host, $db_user, $db_password);

    $conn = mysqli_connect($db_host, $db_user, $db_password, "testDB");

    // Check for successful connection 
    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error()); 
    }
    echo "Connected successfully2";

    //Create a new table if it doesn't already exist x 3 (customers, products, order_lookup)
    $sql = "CREATE TABLE IF NOT EXISTS customers (
        customer_id int(11) NOT NULL AUTO_INCREMENT,
        firstName varchar(100) NOT NULL,
        lastName varchar(100) NOT NULL,
        billingAddress varchar(255) NOT NULL,
        billingCity varchar(100) NOT NULL,
        billingProvince varchar(100) NOT NULL,
        billingCountry varchar(100) NOT NULL,
        billingPostal varchar(20) NOT NULL,
        shippingAddress varchar(255) NOT NULL,
        shippingCity varchar(100) NOT NULL,
        shippingProvince varchar(100) NOT NULL,
        shippingCountry varchar(100) NOT NULL,
        shippingPostal varchar(20) NOT NULL,
        PRIMARY KEY (customer_id)           
    ) CHARSET=utf8mb4";
    mysqli_query($conn, $sql);

    $sql = "CREATE TABLE IF NOT EXISTS products (
        product_id int(11) NOT NULL AUTO_INCREMENT,
        product_name varchar(255) NOT NULL,
        product_price DECIMAL(10,2) NOT NULL,
        PRIMARY KEY (product_id)           
    ) CHARSET=utf8mb4";
    mysqli_query($conn, $sql);

    $sql = "CREATE TABLE IF NOT EXISTS order_lookup (
        cart_id int(11) NOT NULL AUTO_INCREMENT,
        customer_id INT NOT NULL,
        product_id INT NOT NULL,
        quantity INT NOT NULL,
        PRIMARY KEY (cart_id)           
    ) CHARSET=utf8mb4";
    mysqli_query($conn, $sql);

    //ALTER TABLE (i.e. change existing or add new field)
    $sql = "ALTER TABLE customers ADD sameAsBilling TINYINT 
            NOT NULL AFTER billingPostal";
    mysqli_query($conn, $sql);

    //INSERT DATA INTO EACH TABLE
    $sql = "TRUNCATE TABLE customers";
    mysqli_query($conn, $sql);

    $sql = "TRUNCATE TABLE products";
    mysqli_query($conn, $sql);

    $sql = "TRUNCATE TABLE order_lookup";
    mysqli_query($conn, $sql);
    //$billingAddress = $_POST['billingAddress'];
    //$billingAddress = 
             //mysqli_real_escape_string($conn, $billingAddress);
    $sql="INSERT INTO customers 
    (firstName, lastName, billingAddress, billingCity, 
     billingProvince, billingCountry, billingPostal, sameAsBilling, 
     shippingAddress, shippingCity, 
     shippingProvince, shippingCountry, shippingPostal) 
    VALUES ('Rory', 'Andrews', 'here', 'Sydney', 'NS', 'CA', 'B', 
    1, '', '', '', '', '')";
    mysqli_query($conn, $sql);

    $sql="INSERT INTO customers 
    (firstName, lastName, billingAddress, billingCity, 
     billingProvince, billingCountry, billingPostal, sameAsBilling, 
     shippingAddress, shippingCity, 
     shippingProvince, shippingCountry, shippingPostal) 
    VALUES ('Angus', 'Andrews', 'here', 'Sydney', 'NS', 'CA', 'B', 
    1, '', '', '', '', '')";
    mysqli_query($conn, $sql);

    $sql = "INSERT INTO products 
            (product_name, product_price) 
            VALUES 
            ('widget 1', 6.99), ('widget 2', 7.99)";
    echo "Query: ".$sql."<br />";
    mysqli_query($conn, $sql);

    $sql = "INSERT INTO order_lookup 
            (customer_id, product_id, quantity) 
            VALUES 
            (1, 1, 5), (1, 2, 6), (2, 2, 10)";
    mysqli_query($conn, $sql);



/*
    $firstName = $_POST['firstName'];
    $firstName = 
        mysqli_real_escape_string($conn, $firstName);
    $sql = "INSERT INTO customers (firstName) 
            VALUES (?)";
    $dbo->commit($sql, array($firstname));

    $Matt = new app\Customer;
    $Matt->firstName = "Matt";
    $Matt->lastName = "Read-y-or-not";
    $Matt->save();

    $Dan = app\Customer::find(2);
*/

    //UPDATE DATA WITHIN EACH TABLE
    $sql2 = "UPDATE customers 
            SET firstName = 'Alex', lastName='Petroleum'
            WHERE customer_id=2";
    mysqli_query($conn, $sql2);

    $sql = "SELECT * 
            FROM customers 
            ORDER BY customer_id DESC";
    $results = mysqli_query($conn, $sql);
    while ($row = mysqli_fetch_assoc($results)) {
        $id = $row['customer_id'];
        $fName = $row['firstName'];
        $lName = $row['lastName'];
        //echo "Hello $fName $lName <br />";
        $sql = "UPDATE customers 
                SET lastName = 'Mountain'
                WHERE customer_id=$id";
        mysqli_query($conn, $sql);
    } 

    //DELETE DATA WITH EACH TABLE
    /*
    $sql = "DELETE FROM customers 
            WHERE customer_id=1";
    mysqli_query($conn, $sql);
    */

    //SELECT DATA FROM EACH TABLE
    $sql = "SELECT * 
            FROM order_lookup 
            WHERE customer_id = 1";
    $results = mysqli_query($conn, $sql);
    while ($row = mysqli_fetch_assoc($results)) {
        $customer_id = $row['customer_id'];
        $product_id = $row['product_id'];
        $quantity = $row['quantity'];

        $sql = "SELECT * 
                FROM customers 
                WHERE customer_id = $customer_id";
        $results2 = mysqli_query($conn, $sql);
        while ($row = mysqli_fetch_assoc($results2)) {
            $firstName = $row['firstName'];
            $lastName = $row['lastName'];
        }

        $sql = "SELECT * 
                FROM products 
                WHERE product_id = $product_id";
        $results2 = mysqli_query($conn, $sql);
        while ($row = mysqli_fetch_assoc($results2)) {
            $product_name = $row['product_name'];
            $product_price = $row['product_price'];
        }

        echo "$firstName $lastName: $product_name $$product_price ($quantity)";
        echo "<br />";
    }
    //SELECT DATA MERGING ALL 3 TABLES
    $sql = "SELECT * 
            FROM order_lookup, customers, products 
            WHERE 
                order_lookup.customer_id = customers.customer_id 
            AND
                order_lookup.product_id = products.product_id
            AND 
                order_lookup.customer_id = 1
            ";
    $sql = "SELECT cart_id, firstName, lastName, 
                   order_lookup.product_id, product_name,
                   product_price, quantity
            FROM order_lookup, customers, products 
            WHERE 
                order_lookup.customer_id = customers.customer_id 
            AND
                order_lookup.product_id = products.product_id
            AND 
                order_lookup.customer_id = 1
    ";
    while ($row = mysqli_fetch_assoc($results)) {
        $cart_id = $row['cart_id'];
        $product_id = $row['product_id'];
        $product_name = $row['product_name'];
        $product_price = $row['product_price'];
        $quantity = $row['quantity'];
        $firstName = $row['firstName'];
        $lastName = $row['lastName'];
        echo "$firstName $lastName: $product_name $$product_price ($quantity)";
        echo "<br />";
    }

    $sql = "SELECT cart_id, firstName, lastName, 
                   order_lookup.product_id, product_name,
                   product_price, quantity
            FROM order_lookup
            INNER JOIN products ON order_lookup.product_id = products.product_id
            INNER JOIN customers ON order_lookup.customer_id = customers.customer_id
            WHERE 
                order_lookup.customer_id = customers.customer_id 
            AND
                order_lookup.product_id = products.product_id
            AND 
                order_lookup.customer_id = 1
    ";
    while ($row = mysqli_fetch_assoc($results)) {
        $cart_id = $row['cart_id'];
        $product_id = $row['product_id'];
        $product_name = $row['product_name'];
        $product_price = $row['product_price'];
        $quantity = $row['quantity'];
        $firstName = $row['firstName'];
        $lastName = $row['lastName'];
        echo "33 $firstName $lastName: $product_name $$product_price ($quantity)";
        echo "<br />";
    }

    //close the database connection
    mysqli_close($conn); 
?>
