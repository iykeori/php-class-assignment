<?php
    include("Employee.php");

    $person1 = new Employee();
    $person1-> display();
    $person1->setID(11);
    $person1->display();

    $person2 = new Employee("sam", "wilson");
    $person2->display();

    $person3 = new Employee(3000,"sam", "wilson" ,"NY", 1,  50);
    $person3->display();