<?php 
    include("Person.php");


    $person1 = new Person();
    $person1->display();
    $person1->setID(5);
    $person1->setFirstName("ik");
    $person1->setLastName("you");
    $person1->setAddress("there");
    $person1->display();

    $person2 = new Person("Gwen","Stacey");
    $person2-> display();

    $person3 = new Person(1,"Gwen","Stacey", "Russia");
    $person3-> display();
    
