<?php

//1. Create a HTML form that allows a user to enter and submit a real (decimal) number. 
//After pressing submit, use PHP to retrieve the number and store as a variable. 
//Display the result when ceil, floor, and round functions are applied to that variable. 
//Test your code with a variety of real numbers.

    //retrieve the data sent via the METHOD (post)
    $number = "";
    //check that the form has been submitted
    $errors = false;
    if ( isset( $_POST["form1"] ) ) {
        //if it has, retrieve each field
        $number = $_POST["number"];
        //check your retrieved data for errors       
        $error_code = 0;
        if ( $number == null || empty($number) ) { 
            $errors = true; 
            $error_code=1;
        }

    }

    //if there are errors redisplay the form
    if (! isset( $_POST["form1"] ) || $errors) { 
?>
<!DOCTYPE html>
<html>
    <head>
        <title>My Form</title>
    </head>
    <body>
        <p>Please enter a number in the box provided
        </p>
        <form action="" method="post">
            
            Number*: <input type="text" name="number" value="<?php echo $number; ?>" />
            <?php if ( isset($_POST["form1"]) && empty($number)) echo " *required "; ?><br />

            <input type="submit" name="form1" value="Submit" />
        </form>
    </body>
</html>
<?php
    } else {
        //otherwise insert into database, etc and 
        //redirect to success page
        echo "CEIL: ".ceil($number)."<br />";
        echo "FLOOR: ".floor($number)."<br />";
        echo "ROUND: ".round($number)."<br />";
    }

?>