<?php  include('server.php'); ?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Registration</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<!--   <link rel="stylesheet" type="text/css" href="style.css"> -->
  <link href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css" rel="stylesheet">
<!-- Page level plugin JavaScript-->
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>

<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
  <style>
    .container {
        margin-top: 30px;
    }
    form {
        margin-top: 50px;
    }
</style>

</head>
<body>
<div class="container">


<table class="table table-bordered" id="dataTable" width="50%" cellspacing="0">
    <thead>
     <tr>
        <th>Last Name</th>           
      </tr>
    </thead>

        <tr>
          <td>

			 <?php

			 $results = mysqli_query($db, "SELECT LastName from registered_users"); 

			$last_name_array = [];

			  while ($row = mysqli_fetch_array($results)) { 
			  	array_push($last_name_array, $row['LastName']);
			  }

			  sort($last_name_array);

			foreach ($last_name_array as $LastName) {
				echo $LastName.'<br>';
			}

			?>
		  </td>
	   </tr>

</table>

</div>
</body>
</html>
